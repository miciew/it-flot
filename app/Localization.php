<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Localization extends Model
{
    protected $fillable = [ 'locale_id', 'key', 'text' ];

    protected $primaryKey = null;

    public $incrementing = false;

    // relations

    public function localable()
    {
        return $this->morphTo();
    }

    // scopes

    public function scopeByLocale(Builder $builder, string $locale)
    {
        return $builder->where('locale_id', $locale);
    }

    public function scopeByKey(Builder $builder, string $key)
    {
        return $builder->where('key', $key);
    }
}
