<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Traits\Locale\Localization;


class Category extends Model
{
    use Localization;

    protected $appends = [
        'title',
        'content'
    ];

    // relations

    public function adverts()
    {
        return $this->hasMany(Advert::class);
    }

    // mutators
    public function getTitleAttribute()
    {
        $loc = $this->localizations()
                    ->byKey('title')
                    ->byLocale( app()->getLocale() )
                    ->first();

        return $loc ? $loc->text : '';
    }

    public function getContentAttribute()
    {
        $loc = $this->localizations()
                    ->byKey('content')
                    ->byLocale( app()->getLocale() )
                    ->first();

        return $loc ? $loc->text : '';
    }
}
