<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Traits\Locale\Localization;


class Advert extends Model
{
    use Localization;

    protected $fillable = [
        'city_id',
        'category_id'
    ];

    protected $appends = [
        'title',
        'content'
    ];

    // relations

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    // mutators

    public function getTitleAttribute()
    {
        $loc = $this->localizations()
            ->byLocale( app()->getLocale() )
            ->byKey('title')
            ->first();

        return $loc ? $loc->text : '';
    }

    public function getContentAttribute()
    {
        $loc = $this->localizations()
            ->byLocale( app()->getLocale() )
            ->byKey('content')
            ->first();

        return $loc ? $loc->text : '';
    }
}
