<?php

namespace App\Traits\Locale;

use \App\Localization as LocModel;

trait Localization
{
    public function localizations()
    {
        return $this->morphMany(LocModel::class, 'localable');
    }
}
