<?php

namespace App\Http\Middleware;

use Closure;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $defaultSystemLocale = config('app.locale');

        $locale = $request->cookie('locale');

        if( ! \App\Locale::where('id', $locale)->exists() ) {
            $locale = $defaultSystemLocale;
        }

        app()->setLocale($locale);

        return $next($request);
    }
}
