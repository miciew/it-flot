<?php

namespace App\Http\Controllers;

use App\Locale;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locales = Locale::orderBy('sort', 'asc')->get();

        return  $locales;
    }

    public function switcher()
    {
        $locales = Locale::whereNotIn('id', [
            app()->getLocale()
        ])
         ->orderBy('sort', 'asc')->get();

        return  $locales;
    }
}
