<?php

namespace App\Http\Controllers;

use App\Advert;
use App\City;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdvertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')
            ->except([
                'index',
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $advertQuery = Advert::query();

        $cityIDs = collect();

        if( $request->has('city') )
        {
            $city = City::where('slug', $request->get('city'))
                ->firstOrFail();

            $cityIDs->push($city->id);
        }
        else if( $request->has('country') ) {

            $country = Country::where('slug', $request->get('country'))
                ->with('cities')
                ->firstOrFail();

            $cityIDs = collect($country->cities->modelKeys());
        }

        if( $cityIDs->isNotEmpty() ) {
            $advertQuery->whereIn('city_id', $cityIDs->toArray());
        }

        $adverts = $advertQuery
            ->with([
                'city.country',
                'category'
            ])
            ->get();

        return  response()->json($adverts);
    }

    public function personal()
    {
        $adverts = auth()->user()->adverts()->with([
            'city.country',
            'category'
        ])->get();

        return  response()->json($adverts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'localizations.ru.title' => 'required',
                'localizations.en.title' => 'required',
                'localizations.ru.content' => 'required',
                'localizations.en.content' => 'required',
                'city_id' => 'required',
                'category_id' => 'required',
        ]);

        DB::beginTransaction();

        try
        {
            /** @var Advert $advert */
            $advert = auth()->user()->adverts()->create($request->all());

            foreach ($request->get('localizations') as $locale => $parameters) {

                $forCreatelocalizations = [];
                $localizations = [];
                foreach ($parameters as $key => $content) {

                    $localizations['locale_id'] = $locale;
                    $localizations['key'] = $key;
                    $localizations['text'] = $content;

                    $forCreatelocalizations[] = $localizations;
                }

                $advert->localizations()->createMany($forCreatelocalizations);
            }


            DB::commit();;
        }
        catch (\Exception $ex)
        {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'errors' => [
                    $ex->getMessage()
                ]
            ])->setStatusCode(500);
        }

        return response()->json([
            'success' => true,
            'advert'=> $advert
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function show(Advert $advert)
    {
        return  response()->json($advert->load([
            'localizations'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advert $advert)
    {
        if ( ! auth()->user()->can('update', $advert)) {
            return response()->json([
                'success' => false,
                'errors' => [
                    'access denied '
                ]
            ], 403);
        }

        $updated = $advert->update($request->all());

        return response()->json([
            'success' => $updated
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advert $advert)
    {
        if ( ! auth()->user()->can('delete', $advert)) {
            return response()->json([
                'success' => false,
                'errors' => [
                    'access denied '
                ]
            ], 403);
        }

        $deleted = $advert->delete();

        return  response()->json([
            'success' => $deleted
        ]);
    }
}
