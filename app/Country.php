<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Locale\Localization;

class Country extends Model
{
    use Localization;

    protected $fillable = [
        'slug',
        'sort'
    ];

    protected $appends = [
        'title'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    // relations

    public function cities()
    {
        return $this->hasMany(City::class);
    }


    // mutators

    public function getTitleAttribute()
    {
        $loc = $this->localizations()
                    ->byKey('title')
                    ->byLocale( app()->getLocale() )
                    ->first();

        return $this->attributes['title'] = $loc ? $loc->text : '';
    }


    // scopes



}
