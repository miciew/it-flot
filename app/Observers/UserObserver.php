<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Str;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function creating(User $user)
    {

        if( strlen($user->api_token) <= 0 ) {
            $user->api_token = Str::random(60);
        }
    }
}
