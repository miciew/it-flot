<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    protected $fillable = [ 'id' ];

    protected $keyType = 'string';
}
