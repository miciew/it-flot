<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Traits\Locale\Localization;


class UserType extends Model
{
    use Localization;

    protected $fillable = [
        'sort'
    ];

    protected $appends = [
        'title'
    ];

    // relations

    public function users()
    {
        return $this->hasMany(User::class);
    }

    // mutators

    public function getTitleAttribute()
    {
        $loc = $this->localizations()
            ->byKey('title')
            ->byLocale( app()->getLocale() )
            ->first();

        return $loc ? $loc->text : '';
    }
}
