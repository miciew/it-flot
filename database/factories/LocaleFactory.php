<?php

use Illuminate\Support\Str;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Locale;
use Faker\Generator as Faker;

$factory->define(Locale::class, function (Faker $faker) {
    return [
        'id' => Str::random(2)
    ];
});
