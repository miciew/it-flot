<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localizations', function (Blueprint $table) {

            $table->string('locale_id');
            $table->foreign('locale_id')->on('locales')->references('id');

            $table->morphs('localable');

            $table->string('key')->default('title');

            $table->longText('text')->nullable();
            $table->timestamps();

            $table->primary(['locale_id', 'key', 'localable_type', 'localable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localizations');
    }
}
