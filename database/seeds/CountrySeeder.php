<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getCountries() as $slug => $countriesLoc) {

            /** @var \App\UserType $userType */
            $country = \App\Country::firstOrCreate([
                'slug' => $slug
            ]);

            foreach ($countriesLoc as $countryLoc) {
                $loc = \App\Localization::whereHasMorph(
                    'localable',
                    [\App\Country::class],
                    function (\Illuminate\Database\Eloquent\Builder $builder) use ($countryLoc) {
                        $builder->where('text', 'like', $countryLoc['text'])
                                ->where('key', $countryLoc['key']);
                    }
                )->first();

                if ($loc) {
                    continue;
                }

                $country->localizations()
                        ->create($countryLoc);
            }


            foreach ($this->cities($slug) as $citySlug => $cityLocs) {

                $city = $country->cities()->create([
                    'slug' => $citySlug
                ]);

                foreach ($cityLocs as $cityLoc) {
                    $city->localizations()->create($cityLoc);
                }
            }

        }

    }

    protected function cities($countrySlug)
    {
        $cities = [
            'russia' => [
                'moscow' => [
                    [
                        'locale_id' => 'ru',
                        'key' => 'title',
                        'text' => 'Москва',
                    ],
                    [
                        'locale_id' => 'en',
                        'key' => 'title',
                        'text' => 'Moscow',
                    ],
                ],
                'grozny' => [
                    [
                        'locale_id' => 'ru',
                        'key' => 'title',
                        'text' => 'Грозный',
                    ],
                    [
                        'locale_id' => 'en',
                        'key' => 'title',
                        'text' => 'Grozny',
                    ],
                ],
                'volgograd' => [
                    [
                        'locale_id' => 'ru',
                        'key' => 'title',
                        'text' => 'Волгоград',
                    ],
                    [
                        'locale_id' => 'en',
                        'key' => 'title',
                        'text' => 'Volgograd',
                    ],
                ]
            ],

            'england' => [
                'london' => [
                    [
                        'locale_id' => 'ru',
                        'key' => 'title',
                        'text' => 'Лондон',
                    ],
                    [
                        'locale_id' => 'en',
                        'key' => 'title',
                        'text' => 'London',
                    ],
                ],
                'liverpol' => [
                    [
                        'locale_id' => 'ru',
                        'key' => 'title',
                        'text' => 'Ливерпуль',
                    ],
                    [
                        'locale_id' => 'en',
                        'key' => 'title',
                        'text' => 'Liverpol',
                    ],
                ],
            ]
        ];


        return $cities[$countrySlug];
    }

    protected function getCountries()
    {
        return [
            'russia' => [
                [
                    'locale_id' => 'ru',
                    'key' => 'title',
                    'text' => 'Россия',
                ],
                [
                    'locale_id' => 'en',
                    'key' => 'title',
                    'text' => 'Russia',
                ],
            ],
            'england' => [
                [
                    'locale_id' => 'ru',
                    'key' => 'title',
                    'text' => 'Англия',
                ],
                [
                    'locale_id' => 'en',
                    'key' => 'title',
                    'text' => 'England',
                ],
            ],
        ];
    }
}
