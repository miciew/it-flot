<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Builder;
use App\Localization;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getCategories() as $categoryLocs)
        {
            /** @var \App\UserType $userType */
            $categoty = \App\Category::create();

            $categoty->localizations()
                     ->createMany($categoryLocs);
        }
    }

    protected function getCategories()
    {
        return [
            [
                [
                    'locale_id' => 'ru',
                    'key' => 'title',
                    'text' => 'Авто',
                ],
                [
                    'locale_id' => 'en',
                    'key' => 'title',
                    'text' => 'Auto',
                ],
            ],
            [
                [
                    'locale_id' => 'ru',
                    'key' => 'title',
                    'text' => 'Недвижимость',
                ],
                [
                    'locale_id' => 'en',
                    'key' => 'title',
                    'text' => 'Real estate',
                ],
            ]
        ];
    }
}
