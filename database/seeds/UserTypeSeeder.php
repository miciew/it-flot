<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Builder;


class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getTypes() as $types)
        {
            /** @var \App\UserType $userType */
            $userType = \App\UserType::create([
                'sort' => 100
            ]);

            $userType->localizations()
                     ->createMany($types);
        }

    }

    protected function getTypes()
    {
        return [
            [
                [
                    'locale_id' => 'ru',
                    'key' => 'title',
                    'text' => 'Физическое лицо'
                ],
                [
                    'locale_id' => 'en',
                    'key' => 'title',
                    'text' => 'Individual'
                ]
            ],
            [
                [
                    'locale_id' => 'ru',
                    'key' => 'title',
                    'text' => 'Юридическое лицо'
                ],
                [
                    'locale_id' => 'en',
                    'key' => 'title',
                    'text' => 'Entity'
                ]
            ],
        ];
    }
}
