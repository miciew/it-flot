<?php

use Illuminate\Database\Seeder,
    App\Locale as Loc;

class LocaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getListRaw() as $locale)
        {
            Loc::firstOrCreate($locale);
        }
    }

    protected function getListRaw()
    {
        return [
            [
                'id' => 'ru',
                'sort' => 100
            ],
            [
                'id' => 'en',
                'sort' => 200
            ],
        ];
    }
}
