<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAttributes = [
            'email' => 'admin@admin.com',
            'name' => 'Admin',
            'password' => Hash::make('secret')
        ];

        \App\User::firstOrCreate([
            'email' => $userAttributes['email'],
            'api_token' => Str::random(60)
        ], $userAttributes);
    }
}
