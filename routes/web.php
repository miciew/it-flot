<?php


Route::get('/refresh-csrf-token', function() {
    return response()->json([
        'csrfToken' => csrf_token()
    ]);
});

Route::get('/countries', 'CountryController@index');

Route::get('/country/{country}/cities', 'CityController@index');

Route::get('/locales', 'LocaleController@index');
Route::get('/locales/switcher', 'LocaleController@switcher');

Route::get('/categories', 'CategoryController@index');

Route::get('/user-types', 'UserTypeController@index');

Route::get('/adverts/all', 'AdvertController@index');

Route::post('/set-locale/{locale}', function(\App\Locale $locale) {

    return response([])->withCookie('locale', $locale->id, 24 * 60);
});

// при обновлении стр на фронте, чтоб не было 404
Route::get('/personal/adverts/create', function(){
    return view('spa');
});
Route::get('/personal/adverts', function(){
    return view('spa');
});
Route::get('/adverts', function(){
    return view('spa');
});
Route::get('/adverts/{id}/edit', function($id){
    return view('spa');
});

// Localization
Route::get('/js/lang.js', function () {

    $locale = app()->getLocale();

    $localizations = Cache::rememberForever("lang.{$locale}.js", function () use ($locale){
        $files = Storage::disk('localization')
                        ->allFiles($locale);

        $localizations = collect();
        foreach ($files as $file) {

            $fileName = basename($file, '.php');

            $localizations->put($fileName, require resource_path("lang/{$file}"));
        }

        return $localizations;
    });

    $loc = 'window.i18n = ' . json_encode($localizations);

    return response($loc)
        ->header('Content-Type', 'application/javascript');

});




Route::get('/{any?}', 'SpaController@index');


Auth::routes();

