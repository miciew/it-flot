@extends('layouts.app')

@section('content')

    <main-menu></main-menu>
    <router-view></router-view>
@endsection
