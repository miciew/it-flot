
import Auth from "./components/Auth/AuthComponent";
import Home from './components/HomeComponent';
import AdvertCreate from './components/Adverts/FormComponent';
import Register from './components/Auth/RegisterComponent';
import AdvertList from './components/Adverts/ListComponent';
import PersonalListComponent from './components/Adverts/PersonalListComponent';
import Edit from './components/Adverts/EditComponent';
import AdvertFilter from './components/Adverts/FilterComponent';


export default {
    mode: 'history',
    routes: [
        {
            path: '/:country?/:city?',
            name: 'home',
            component: AdvertList,
        },

        {
            path: '/login',
            name: 'auth',
            component: Auth,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },

        {
            path: '/personal/adverts/create',
            name: 'advert.create',
            component: AdvertCreate,
        },

        {
            path: '/personal/adverts',
            name: 'advert.personal.list',
            component: PersonalListComponent,
        },

        {
            path: '/adverts',
            name: 'advert.list',
            component: AdvertList,
        },

        {
            path: '/adverts/:id/edit',
            name: 'advert.edit',
            component: Edit,
        }
    ]
}
