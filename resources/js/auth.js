let auth = {
    state: {
        user: window.application.user || null
    },

    getters: {

        user: (state) => {
            return state.user;
        }
    },

    mutations: {
        setUser(state, user) {
            state.user = user;
        },

        resetUser(state) {
            state.user = null;
        }
    },

    actions: {

        login({ commit }, auth) {

            return new Promise(function (resolve, reject) {
                axios.post(
                    '/login',
                    {
                        email: auth.email,
                        password: auth.password
                    })
                     .then((response) => {
                         commit('setUser', response.data.user);
                         window.application.user = response.data.user;

                         resolve(response);
                     })
                     .catch((error) => {
                         commit('setUser', null);

                         reject(error);
                     });

            });
        },


        logout ({ commit }) {
            return new Promise(function (resolve, reject) {
                axios.post('/logout')
                     .then( (response) => {
                         commit('resetUser');
                         window.application.user = null;
                         resolve(response);
                     })
                     .catch((error) => {
                         reject(error);
                     })
            });
        },

        register({ commit }, registration_params) {

            return new Promise(function (resolve, reject) {
                axios.post('/register', registration_params)
                     .then( (response) => {

                         commit('setUser', response.data.user);

                         resolve(response);
                     })
                     .catch( (error) => {
                         commit('setUser', null);
                         reject(error);
                     });
            });
        }
    }
};

export default auth;
