<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, повторите попытку через: :seconds, секунды.',
    'authorized' => 'Успешный вход',
    'unauthorized' => 'Успешный выход',
    'email' => 'эл почта',
    'password' => 'пароль',
    'remember' => 'запомнить меня',
    'logout' => 'выйти',
    'password_confirmation' => 'подтвердить пароль',
    'registration' => 'зарегистрироваться',
    'name' => 'имя',
    'need_auth' => 'нужна авторизация',

];
