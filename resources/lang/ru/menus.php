<?php

return [
    'home' => 'Главная',
    'advert_list' => 'Все объявления',
    'my_adverts' => 'Мои объявления',
    'create_advert' => 'Добавить объявление',
    'login_logout' => 'Войти/Выйти',
    'register' => 'Регистрация',
    'filter' => 'фильтр',
];
