<?php

return [

    'create' => 'Create new advert',
    'categories' => 'Choice category',
    'countries' => 'Choice countries',
    'cities' => 'Choice cities',
    'content' => 'content',
    'submit' => 'submit',
    'title' => 'title',
    'created' => 'created!',
    'created_desc' => 'Your advert was success created',
    'edit' => 'edit',
];
