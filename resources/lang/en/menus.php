<?php

return [
    'home' => 'Index',
    'advert_list' => 'all adverts',
    'my_adverts' => 'my adverts',
    'create_advert' => 'create',
    'login_logout' => 'login/logout',
    'register' => 'register',
    'filter' => 'filter',
];
